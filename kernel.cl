typedef struct	s_params
{
	double xmin;
	double ymin;
	double xoff;
	double yoff;
	double scale;
	double cx;
	double cy;
	size_t max_iterations;
	size_t imgx;
	size_t imgy;
	int    palette;
	char   pattern;
}				t_params;

__kernel void mandelbrot(__global int *output, __global int* colors, __global t_params *p)
{
	double x = 0;
	double y = 0;
	double xnew;
	size_t iter;
	iter = 0;
	size_t id;
	id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while ((x * x + y * y) < 4 && iter < p->max_iterations)
	{
		xnew = (x * x - y * y) + re;
		y = (2 * x * y) + im;
		x = xnew;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void ship(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew;
	double x = 0;
	double y = 0;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		if (x * x + y * y >= 4)
			break ;
		xnew = fabs(x * x - y * y + re);
		y = fabs(2 * x * y) + im;
		x = xnew;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void ship2(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, x, y;
	x = 0;
	y = 0;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		if (x * x + y * y >= 4)
			break ;
		xnew = fabs(x * x - y * y + re);
		y = (2 * -fabs(x) * y) + im;
		x = xnew;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void ship3(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, x, y;
	x = 0;
	y = 0;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		if (x * x + y * y >= 4)
			break ;
		xnew = fabs(x * x + y * y + re);
		y = (2 * -fabs(x) * y) + im;
		x = xnew;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void ship4(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, x, y;
	x = 0;
	y = 0;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		if (x * x + y * y >= 4)
			break ;
		xnew = fabs(x * x - y * y) + re;
		y = (-2 * fabs(y) * x) + im;
		x = xnew;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void julia(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, re2, im2;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		re2 = re * re;
		im2 = im * im;
		if (re2 + im2 >= 4)
			break ;
		xnew = re2 - im2;
		im = (2 * re * im) + p->cy;
		re = xnew + p->cx;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void julia2(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, re2, im2;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		re2 = re * re;
		im2 = im * im;
		if (re2 + im2 >= 4)
			break ;
		xnew = re2 - im2;
		im = (-2 * re * im) + p->cy;
		re = xnew + p->cx;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void julia3(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, re2, im2, pw, truc;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		re2 = re * re;
		im2 = im * im;
		if (re2 + im2 >= 4)
			break ;
		pw = pow(re2 + im2, 1.5);
		truc = 3 * atan2(im, re);
		xnew = pw * cos(truc);
		im = pw * sin(truc) + p->cy;
		re = xnew + p->cx;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void julia4(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, re2, im2, pw, truc;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		re2 = re * re;
		im2 = im * im;
		if (re2 + im2 >= 4)
			break ;
		pw = pow(re2 + im2, 2);
		truc = 4 * atan2(im, re);
		xnew = pw * cos(truc);
		im = pw * sin(truc) + p->cy;
		re = xnew + p->cx;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void julia5(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, re2, im2, pw, truc;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		re2 = re * re;
		im2 = im * im;
		if (re2 + im2 >= 4)
			break ;
		pw = pow(re2 + im2, 2.5);
		truc = 5 * atan2(im, re);
		xnew = pw * cos(truc);
		im = pw * sin(truc) + p->cy;
		re = xnew + p->cx;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void julia6(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, re2, im2, pw, truc;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		re2 = re * re;
		im2 = im * im;
		if (re2 + im2 >= 4)
			break ;
		pw = pow(re2 + im2, 3);
		truc = 6 * atan2(im, re);
		xnew = pw * cos(truc);
		im = pw * sin(truc) + p->cy;
		re = xnew + p->cx;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void julia7(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, re2, im2, pw, truc;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		re2 = re * re;
		im2 = im * im;
		if (re2 + im2 >= 4)
			break ;
		pw = pow(re2 + im2, 3.5);
		truc = 7 * atan2(im, re);
		xnew = cos(truc) * pw;
		im = sin(truc) * pw + p->cy;
		re = xnew + p->cx;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}

__kernel void tricorn(__global int *output, __global int* colors, __global t_params *p)
{
	double xnew, re2, im2;
	size_t iter;
	iter = 0;
	size_t id = get_global_id(0);
	double re = ((id % p->imgx) + (p->imgx / p->xmin)) * (p->scale / p->imgx) + p->xoff;
	double im = ((id / p->imgx) + (p->imgy / p->ymin)) * (p->scale / p->imgx) - p->yoff;
	while (iter < p->max_iterations)
	{
		re2 = re * re;
		im2 = im * im;
		if (re2 + im2 >= 4)
			break ;
		xnew = re2 - im2 + re;
		im = (-2 * re * im) + im;
		re = xnew;
		iter++;
	}
	if (iter < p->max_iterations)
		output[id] = colors[(p->pattern ? ((int)(iter * 16 / p->max_iterations)) : (iter % 16))];
	else
		output[id] = 0;
}