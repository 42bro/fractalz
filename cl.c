#include "fractalz.h"

int is_valid_set(char *str)
{
    return (!(strcmp(str, "mandelbrot") && strcmp(str, "julia") && strcmp(str, "tricorn") && strcmp(str, "julia2") && strcmp(str, "julia3") && strcmp(str, "julia4") && strcmp(str, "julia5") && strcmp(str, "julia6") && strcmp(str, "julia7") && strcmp(str, "ship") && strcmp(str, "ship2") && strcmp(str, "ship3") && strcmp(str, "ship4")));
}

int invalid_args(int ac, char **av)
{
    return (ac != 2 || !av[1][0] || !is_valid_set(av[1]));
}

void read_file(t_f *f)
{
    int fd;
    int ret;
    uint32_t offset;

    fd = 0;
    offset = 0;
    if ((fd = open("kernel.cl", O_RDONLY)) < 3)
        gros_fail(f, "can't open file", fd);
    if (!(f->kernel = (char *)malloc(BUFF_SIZE * sizeof(char))))
        gros_fail(f, "malloc sucks", 69);
    while ((ret = read(fd, f->kernel + offset, 60)) > 0)
    {
        offset += ret;
    }
    f->kernel[offset] = 0;
    if (ret < 0)
        gros_fail(f, "can't read file", 69);
    close(fd);
}

void init_sdl(t_f *f)
{
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO))
        gros_fail(f, "SDL Init Fail", 69);
    if (!(f->win = SDL_CreateWindow("fractalz", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIN_W, WIN_H, SDL_WINDOW_SHOWN)))
        gros_fail(f, (const char *)SDL_GetError(), 69);
    if (!(f->ren = SDL_CreateRenderer(f->win, -1, SDL_RENDERER_SOFTWARE)))
        gros_fail(f, (const char *)SDL_GetError(), 69);
    if (!(f->pix = SDL_CreateTexture(f->ren, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, WIN_W, WIN_H)))
        gros_fail(f, (const char *)SDL_GetError(), 69);
}

void free_mem(t_f *f)
{
    int i;

    i = -1;
    clReleaseMemObject(f->cl.buf_pixels);
    clReleaseMemObject(f->cl.buf_colors);
    clReleaseMemObject(f->cl.buf_params);
    clReleaseKernel(f->cl.kern);
    clReleaseProgram(f->cl.prog);
    clReleaseCommandQueue(f->cl.q);
    clReleaseContext(f->cl.con);
    clReleaseDevice(f->cl.id);
    while (++i < PALETTES)
        free(f->colors[i]);
    free(f->colors);
    //if (f->kernel)
    //   free(f->kernel);
    if (f->pix)
        SDL_DestroyTexture(f->pix);
    if (f->ren)
        SDL_DestroyRenderer(f->ren);
    if (f->win)
        SDL_DestroyWindow(f->win);
    if (f)
        free(f);
    SDL_Quit();
    exit(0);
}

void gros_fail(t_f *f, const char *str, int err)
{
    printf("ERROR %s // code #%d \n", str, err);
    free_mem(f);
}

t_params kernel_data(t_f *data)
{
    t_params ret;

    ret.max_iterations = data->max_iterations;
    ret.xmin = data->xmin;
    ret.ymin = data->ymin;
    ret.xoff = data->xoff;
    ret.yoff = data->yoff;
    ret.scale = data->scale;
    ret.cx = data->cx;
    ret.cy = data->cy;
    ret.pattern = data->pattern;
    ret.palette = data->palette;
    return (ret);
}

void init_other_stuff(t_f *f)
{
    int i;

    i = -1;
    if (!(f->colors = (int **)malloc(sizeof(int *) * PALETTES)))
        gros_fail(f, "nique ta mere", 69);
    while (++i < PALETTES)
        if (!(f->colors[i] = (int *)malloc(sizeof(int) * 16)))
            gros_fail(f, "nique ta mere", 69);
    f->max_iterations = 64;
    f->xmin = -2.0;
    f->ymin = -2.0;
    f->scale = 4.0;
    f->zoom_lvl = 0.33;
    color_palette(f);
}

void render(t_f *f)
{

    int   *pixels = NULL;
    t_params lite;
    size_t gsize;
    int err = 0;
    uint32_t acc = SDL_PIXELFORMAT_ARGB32;

    gsize = WIN_W * WIN_H;
    lite = kernel_data(f);
    //SDL_LockTexture(f->pix, NULL, (void**)&pixels, &pitch);
    SDL_QueryTexture(f->pix, &acc, pixels, NULL, NULL);
    err = clEnqueueWriteBuffer(f->cl.q, f->cl.buf_colors, CL_FALSE, 0, (sizeof(int) * 16), f->colors[f->palette], 0, NULL, NULL);
    printf("PROUT\n");
    if (err != CL_SUCCESS)
        gros_fail(f, "clwrite fail", err);

    err = clEnqueueWriteBuffer(f->cl.q, f->cl.buf_params, CL_FALSE, 0, sizeof(lite), &lite, 0, NULL, NULL);
    if (err != CL_SUCCESS)
        gros_fail(f, "clwrite fail", err);
    err = clEnqueueNDRangeKernel(f->cl.q, f->cl.kern, 1, NULL, &gsize, NULL, 0, NULL, NULL);
    if (err != CL_SUCCESS)
        gros_fail(f, "enqueueND fail", err);
    err = clEnqueueReadBuffer(f->cl.q, f->cl.buf_pixels, CL_FALSE, 0, sizeof(int) * gsize, pixels, 0, NULL, NULL);
    if (err != CL_SUCCESS)
        gros_fail(f, "clenqueueread fail", err);
    clFlush(f->cl.q);
    clFinish(f->cl.q);
    //SDL_UnlockTexture(f->pix);
    SDL_UpdateTexture(f->pix, NULL, pixels, WIN_W * sizeof(int));
    SDL_RenderPresent(f->ren);
}

int main(int ac, char **av)
{
    t_f f;
    int err;
    char *log;
    size_t logsize;

    if (invalid_args(ac, av))
        exit(0);
    err = 0;
    read_file(&f);
    if ((err = clGetDeviceIDs(NULL, CL_DEVICE_TYPE_GPU, 1, &f.cl.id, NULL)) != CL_SUCCESS)
        gros_fail(&f, "deviceid fail", err);
    f.cl.con = clCreateContext(NULL, 1, &f.cl.id, NULL, NULL, &err);
    if (err != CL_SUCCESS)
        gros_fail(&f, "context fail", err);
    f.cl.q = clCreateCommandQueue(f.cl.con, f.cl.id, 0, &err);
    if (err != CL_SUCCESS)
        gros_fail(&f, "queue fail", err);
    f.cl.prog = clCreateProgramWithSource(f.cl.con, 1, (const char **)&f.kernel, NULL, &err);
    if (err != CL_SUCCESS)
        gros_fail(&f, "prog crea fail", err);
    clBuildProgram(f.cl.prog, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS)
        gros_fail(&f, "prog build fail", err);

    clGetProgramBuildInfo(f.cl.prog, f.cl.id, CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize);
    if (!(log = (char *)malloc(sizeof(char) * (logsize + 1))))
        gros_fail(&f, "fkiehfkhz", 69);
    clGetProgramBuildInfo(f.cl.prog, f.cl.id, CL_PROGRAM_BUILD_LOG, logsize, log, NULL);
    log[logsize] = 0;
    printf("%s \n", log);
    f.cl.kern = clCreateKernel(f.cl.prog, av[1], &err);
    if (err != CL_SUCCESS)
        gros_fail(&f, "kernel crea fail", err);
    f.cl.buf_pixels = clCreateBuffer(f.cl.con, CL_MEM_WRITE_ONLY, (sizeof(int) * WIN_W * WIN_H), NULL, &err);
    if (err != CL_SUCCESS)
        gros_fail(&f, "buf crea fail", err);
    f.cl.buf_colors = clCreateBuffer(f.cl.con, CL_MEM_READ_ONLY, (sizeof(int) * 16), NULL, &err);
    if (err != CL_SUCCESS)
        gros_fail(&f, "buf crea fail", err);
    f.cl.buf_params = clCreateBuffer(f.cl.con, CL_MEM_READ_ONLY, (sizeof(t_params)), NULL, &err);
    if (err != CL_SUCCESS)
        gros_fail(&f, "buf crea fail", err);
    clSetKernelArg(f.cl.kern, 0, sizeof(cl_mem), &(f.cl.buf_pixels));
    clSetKernelArg(f.cl.kern, 1, sizeof(cl_mem), &(f.cl.buf_colors));
    clSetKernelArg(f.cl.kern, 2, sizeof(cl_mem), &(f.cl.buf_params));
    init_sdl(&f);
    init_other_stuff(&f);
    while (1)
    {
        while (SDL_PollEvent(&f.e))
        {
            if (f.e.type == SDL_QUIT)
                free_mem(&f);
            else if (f.e.type == SDL_KEYUP)
            {
                if (f.e.key.keysym.sym == SDLK_ESCAPE)
                    free_mem(&f);
            }
            else if (f.e.type == SDL_MOUSEMOTION && !f.lock)
            {
                if (!strncmp(av[1], "julia", 5) && strcmp(av[1], "julia") && strcmp(av[1], "julia2"))
                    f.max_iterations = 32;
                f.cx = f.xoff + ((f.e.motion.x - (WIN_W / 2.0)) / (WIN_W / 2.0)) * f.scale / 2.0;
                f.cy = f.yoff - ((f.e.motion.y - (WIN_H / 2.0)) / (WIN_H / 2.0)) * f.scale / 2.0;
            }
            render(&f);
        }
    }
    printf("NO PB\n");
    free_mem(&f);
    return (0);
}