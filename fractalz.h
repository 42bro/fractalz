/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/03 14:47:15 by fdubois           #+#    #+#             */
/*   Updated: 2019/02/27 16:31:42 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTALZ_H
# define FRACTALZ_H

# include <SDL2/SDL.h>
# include <CL/cl.h>
# include <math.h>
# include <fcntl.h>
# include <signal.h>
# include <time.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define WIN_W				1200
# define WIN_H				800
# define PALETTES			5
# define BUFF_SIZE			16384
# define MIN_ITER			16
# define MAX_ITER			1024
# define PROGRAM_SIZE		16384

# define KEY_ESC			53
# define KEY_MINUS			27
# define KEY_EQUAL			24
# define KEY_O				31
# define KEY_P				35
# define KEY_K				40
# define KEY_L				37
# define KEY_OPEN_BRACKET	33
# define KEY_CLOSE_BRACKET	30
# define KEY_LEFT			123
# define KEY_DOWN			125
# define KEY_RIGHT			124
# define KEY_UP				126
# define KEY_PAGE_UP		116
# define KEY_PAGE_DOWN		121

# define KEY_PRESS_MASK		(1L << 0)
# define KEY_PRESS			(1L << 1)
# define KEY_RELEASE_MASK	(1L << 1)
# define KEY_RELEASE		3
# define MOUSE_MOTION_MASK	(1L << 6)
# define MOUSE_MOTION		6
# define MOUSE_CLICK		(1L << 2)
# define MOUSE_SCROLL_UP	4
# define MOUSE_SCROLL_DOWN	5

typedef struct			s_cl
{
	cl_device_id		id;
	cl_context			con;
	cl_command_queue	q;
	cl_program			prog;
	cl_kernel			kern;
	cl_mem				buf_pixels;
	cl_mem				buf_colors;
	cl_mem				buf_params;
}						t_cl;

typedef struct			s_params
{
	double				xmin;
	double				ymin;
	double				xoff;
	double				yoff;
	double				scale;
	double				cx;
	double				cy;
	size_t				max_iterations;
	size_t				imgx;
	size_t				imgy;
	int					palette;
	char				pattern;
}						t_params;

typedef struct			s_f
{
	int					**colors;
	SDL_Window			*win;
	SDL_Renderer		*ren;
	SDL_Texture			*pix;
	t_cl				cl;
	char				*kernel;
	char				*name;
	SDL_Event			e;
	double				xmin;
	double				ymin;
	double				xoff;
	double				yoff;
	double				scale;
	double				cy;
	double				cx;
	double				zoom_lvl;
	size_t				max_iterations;
	size_t				gsize;
	pid_t				music;
	int					palette;
	char				pattern;
	char				lock;
}						t_f;

void					free_mem(t_f *f);
void					init_sdl(t_f *f);
void 					gros_fail(t_f *f, const char *str, int err);

void					render(t_f *data);
int						is_used_key(char c);
int						is_arrow_key(char c);
t_f						*malloc_de_ouf(void);
void					drop_the_bass(t_f *data);
void					start_mlx(t_f *data);
void					start_opencl(t_f *data);

int						is_valid_set(char *str);
int						invalid_args(int ac, char **av);
void					color_palette(t_f *data);
void					cycle_colors(t_f *data);
void					switch_pattern(t_f *data);
void					switch_palette(t_f *data);
void					move(t_f *data, int key);
void					change_iter_nb(t_f *data, int key);
void					recenter(int x, int y, t_f *data);
int						controls(int key, t_f *data);
int						mousectrl(int key, int x, int y, t_f *data);
int						juliaswitch(int x, int y, t_f *data);
int						stop_music(int key, t_f *data);
void					ft_swap(int *a, int *b);

#endif
